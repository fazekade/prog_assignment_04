﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace Assignment4 {
 
    public sealed partial class PredictionEditor : Page {
        
        #region Field variables

        //TODO: Write XAML comment.
        private Horoscope _horoscope;

        //TODO: Write XAML comment.
        private List<string> _predictions;

        #endregion

        public PredictionEditor() {
            this.InitializeComponent();
            _predictions = new List<string>();
        }

        //TODO: Write XAML comment.
        protected override void OnNavigatedTo(NavigationEventArgs e) {
            _horoscope = e.Parameter as Horoscope;
            DisplayPredictions(0);
            base.OnNavigatedTo(e);
        }

        //TODO: Write XAML comment.
        private void DisplayPredictions(int index) {
            // Get all predictions for the zodiac.
            for (int i = 0; i < _horoscope.ZodiacSigns[index].Predictions.Count; i++) {
                
            }
        }

        //TODO: Write XAML comment.
        private void OnAddPrediction(object sender, TappedRoutedEventArgs e) {

        }

        //TODO: Write XAML comment.
        private void OnSave(object sender, TappedRoutedEventArgs e) {

        }
    }
}
