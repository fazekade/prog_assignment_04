﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Assignment4 {
    [DataContract]
    public class ZodiacSign {

        #region Field variables

        //TODO: Write XML comment.
        [DataMember] private string _name;

        //TODO: Write XML comment.
        [DataMember] private int[] _startDate;

        //TODO: Write XML comment.
        [DataMember] private List<string> _predictions;

        //TODO: Write XML comment.
        [DataMember] private HoroscopePersistService _horoscropePersistService;

        #endregion

        public ZodiacSign(string name, string startDate, string predictions) {
            _name = name;
            _startDate = startDate.Split('.').Select(int.Parse).ToArray();
            _horoscropePersistService = new HoroscopePersistService();
            _predictions = predictions.Split('!').ToList<string>();
        }

        #region Properties

        //TODO: Write XML comment.
        public string Name {
            get { return _name; }
        }

        //TODO: Write XML comment.
        public int[] StartDate {
            get { return _startDate; }
        }

        //TODO: Write XML comment.
        public List<string> Predictions {
            get { return _predictions; }
            set { _predictions = value; }
        }
        #endregion

        //TODO: Write XML comment.
        private void AddPredictions(List<string> newPredictions) {
            for (int i = 0; i < newPredictions.Count; i++) {
                _predictions.Add(newPredictions[i]);
            }
        }

    }
}