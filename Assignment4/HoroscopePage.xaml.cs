﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

namespace Assignment4 {
    public sealed partial class HoroscopePage : Page {
        //TODO: Write xml comment.
        private User _user;

        public HoroscopePage() {
            this.InitializeComponent();
        }

        //TODO: Write xml comment.
        private void OnPageLoaded(object sender, RoutedEventArgs e) {
            // Adjust width of user name, birthday, and zodiac summaries.
            double tempWidth = _Col1.ActualWidth / 2;
            _TextBlock_Name_Label.Width = tempWidth;
            _TextBlock_Name_Value.Width = tempWidth;

            _TextBlock_Birth_Label.Width = tempWidth;
            _TextBlock_Birth_Value.Width = tempWidth;

            _TextBlock_Zodiac_Label.Width = tempWidth;
            _TextBlock_Zodiac_Value.Width = tempWidth;
        }

        //TODO: Write xml comment.
        private void OnClick_EditProfile(object sender, TappedRoutedEventArgs e) {
            //TODO: Figure out how to open splitview on MainPage.xaml.
            // Toggle SplitView visibility.
            //_xaml_splitView_Secondary.IsPaneOpen = !_xaml_splitView_Secondary.IsPaneOpen;
        }

        //TODO: Write xml comment.
        private void OnPageResized(object sender, SizeChangedEventArgs e) {
            // Adjust width of user name, birthday, and zodiac summaries.
            double tempWidth = _Col1.ActualWidth / 2;
            _TextBlock_Name_Label.Width = tempWidth;
            _TextBlock_Name_Value.Width = tempWidth;

            _TextBlock_Birth_Label.Width = tempWidth;
            _TextBlock_Birth_Value.Width = tempWidth;

            _TextBlock_Zodiac_Label.Width = tempWidth;
            _TextBlock_Zodiac_Value.Width = tempWidth;
        }

        //TODO: Write xml comment.
        protected override void OnNavigatedTo(NavigationEventArgs e) {
            // Update page info.
            _user                       = e.Parameter as User;
            _TextBlock_Name_Value.Text  = _user.Name;
            _Img_Avatar.Source          = _user.Avatar.Source as BitmapImage;

            // Display stringified birthday.
            string[] months             = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            string day                  = _user.Birthday.Day.ToString();
            string month                = months[_user.Birthday.Month - 1];
            string year                 = _user.Birthday.Year.ToString();
            _TextBlock_Birth_Value.Text = $"{month} {day}, {year}";

            base.OnNavigatedTo(e);
        }
    }
}
