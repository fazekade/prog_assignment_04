﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Pickers;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

namespace Assignment4 {
    public sealed partial class MainPage : Page {
        #region Field variables

        //TODO: Write xml comment.
        private SplitView _xaml_splitView_Primary;
        
        //TODO: Write xml comment.
        private SplitView _xaml_splitView_Secondary;
        
        //TODO: Write xml comment.
        internal static User _user;

        //TODO: Write xml comment.
        internal static Horoscope _horoscope;

        #endregion

        //TODO: Write xml comment.
        public MainPage() {
            this.InitializeComponent();

            _user = new User();
            _horoscope = new Horoscope();

            _xaml_splitView_Primary = _splitView_Primary;
            _xaml_splitView_Secondary = _splitView_Secondary;
        }

        #region Page

        //TODO: Write xml comment.
        private void OnPageLoaded(object sender, RoutedEventArgs e) {
            // Set Left Pane width equal to width of Column 1.
            _xaml_splitView_Primary.OpenPaneLength = _Col1.ActualWidth;

            // Set Right Pane width equal to width of Column 3.
            _xaml_splitView_Secondary.OpenPaneLength = _Col3.ActualWidth;

            // Load Horoscope subpage into frame.
            _Frame_Subframe.Navigate(typeof(HoroscopePage), _user);

            // Adjust width of user name, birthday, and zodiac summaries.
            //double tempWidth = _Col3.ActualWidth / 2;
            //_TextBlock_Name_Label.Width = tempWidth;
            //_TextBlock_Name_Value.Width = tempWidth;

            //_TextBlock_Birth_Label.Width = tempWidth;
            //_TextBlock_Birth_Value.Width = tempWidth;

            //_TextBlock_Zodiac_Label.Width = tempWidth;
            //_TextBlock_Zodiac_Value.Width = tempWidth;
        }
        
        //TODO: Write xml comment.
        private void OnPageResized(object sender, SizeChangedEventArgs e) {
            // Adjust Left Pane width equal to width of Column 1.
            _xaml_splitView_Primary.OpenPaneLength = _Col1.ActualWidth;

            // Adjust Right Pane width equal to width of Column 3.
            _xaml_splitView_Secondary.OpenPaneLength = _Col3.ActualWidth;

            // Adjust width of user name, birthday, and zodiac summaries.
            //double tempWidth = _Col3.ActualWidth / 2;
            //_TextBlock_Name_Label.Width = tempWidth;
            //_TextBlock_Name_Value.Width = tempWidth;

            //_TextBlock_Birth_Label.Width = tempWidth;
            //_TextBlock_Birth_Value.Width = tempWidth;

            //_TextBlock_Zodiac_Label.Width = tempWidth;
            //_TextBlock_Zodiac_Value.Width = tempWidth;
        }

        #endregion

        #region Left SplitView Pane

        /// <summary>
        /// Event Handler method. Toggles the visibility of the Dashboard sidebar.
        /// Author: Devon Fazekas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _btnHamburger_Click(object sender, RoutedEventArgs e) {
            // Toggle the Dashboard sidebar visibility.
            //_xaml_splitView_Primary.IsPaneOpen = !_xaml_splitView_Primary.IsPaneOpen;
            _splitView_Primary.IsPaneOpen = !_splitView_Primary.IsPaneOpen;
        }

        /// <summary>
        /// Event handler method. Dashboard sidebar navigation.
        /// Author: Devon Fazekas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _listIconBox_SelectionChanged(object sender, SelectionChangedEventArgs e) { }

        #endregion

        #region Right SplitView Pane

        /// <summary>
        /// Event handler method. Opens image file picker, allowing user to change their avatar.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OnTap_EditAvatar(object sender, TappedRoutedEventArgs e) {
            var picker = new Windows.Storage.Pickers.FileOpenPicker();
            picker.ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail;
            picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.PicturesLibrary;
            picker.FileTypeFilter.Add(".jpg");
            picker.FileTypeFilter.Add(".jpeg");
            picker.FileTypeFilter.Add(".png");

            Windows.Storage.StorageFile file = await picker.PickSingleFileAsync();
            if (file != null) {
                var stream = await file.OpenAsync(Windows.Storage.FileAccessMode.Read);
                var bitmap = new BitmapImage();

                bitmap.SetSource(stream);
                _Img_EditAvatar.Source = bitmap;
                _Img_EditAvatar.Width = _Col1.ActualWidth;
            }
        }

        //TODO: Write xml comment.
        private void OnTap_SaveChanges(object sender, TappedRoutedEventArgs e) {
            //TODO: Set calendar default value, so unselection doesn't break code.
            // Validate name (empty or whitespace).
            if (String.IsNullOrWhiteSpace(_txtBox_EditName.Text)) {
                // Invalid entry; highlight invalid fields.
                _txtBox_EditName.BorderBrush = new SolidColorBrush(Colors.Red);

                // Set placeholder text.
                _txtBox_EditName.PlaceholderText = "Please enter a valid name";

                // Clear text.
                _txtBox_EditName.Text = "";
                return;
            }

            // Validate calendar (no selection).
            if (_Calendar.SelectedDates[0] == DateTime.MinValue.Date) {
                // Invalid entry; highlight invalid fields.
                _Calendar.CalendarItemForeground = new SolidColorBrush(Colors.Red);
                return;
            }

            // Update user details.
            _user.Name = _txtBox_EditName.Text;
            _user.Birthday = _Calendar.SelectedDates[0].DateTime;
            _user.Avatar.Source = _Img_EditAvatar.Source as BitmapImage;

            

            //TODO: Don't reset, leave everything as current values. Only update changed values.
            // Reset the Edit Profile pane.
            ClearEditProfile();

            // Collapse secondary splitview.
            _splitView_Secondary.IsPaneOpen = false;

            // Update frame values.
            _Frame_Subframe.Navigate(typeof(HoroscopePage), _user);
        }

        //TODO: Write XAML comment.
        private void ClearEditProfile() {
            _txtBox_EditName.Text = "";
            _txtBox_EditName.PlaceholderText = "Enter your name";
            _Calendar.SelectedDates[0] = DateTime.MinValue.Date;
            _Calendar.CalendarItemForeground = new SolidColorBrush(Colors.Black);
            _Img_EditAvatar.Source = null;
        }

        //TODO: Write xml comment.
        private void OnClick_EditProfile(object sender, TappedRoutedEventArgs e) {
            //TODO: Figure out how to open splitview on MainPage.xaml.
            // Toggle SplitView visibility.
            _xaml_splitView_Secondary.IsPaneOpen = !_xaml_splitView_Secondary.IsPaneOpen;
        }

        #endregion

        private void OnTap_PredictionPage(object sender, TappedRoutedEventArgs e) {
            _Frame_Subframe.Navigate(typeof(PredictionEditor), _horoscope);
        }

        private void OnTap_HoroscopePage(object sender, TappedRoutedEventArgs e) {
            _Frame_Subframe.Navigate(typeof(HoroscopePage), _user);
        }

        private void OnChange_CalendarDate(CalendarView sender, CalendarViewSelectedDatesChangedEventArgs args) {
            _Calendar.CalendarItemForeground = new SolidColorBrush(Colors.Black);
        }
    }
}
