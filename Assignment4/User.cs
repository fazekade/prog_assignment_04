﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace Assignment4
{
   public class User {
        #region Field variables

        //TODO: Write xml comment.
        private string _name;
        //TODO: Write xml comment.
        private DateTime _birthday;
        //TODO: Write xml comment.
        private Image _avatar;


        #endregion

        //TODO: Write xml comment.
        public User() {
            _name = "";
            _birthday = DateTime.Now;
            _avatar = new Image();
        }

        #region Properties

        //TODO: Write xml comment.
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        //TODO: Write xml comment.
        public DateTime Birthday
        {
            get { return _birthday; }
            set { _birthday = value; }
        }

        //TODO: Write xml comment.
        public Image Avatar
        {
            get { return _avatar; }
            set { _avatar = value; }
        }

        #endregion

    }
}
