﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment4 {
    public class Horoscope {

        #region Field variables

        //TODO: Write XAML comment.
        private List<ZodiacSign> _zodiacs;

        //TODO: Write XAML comment.
        private HoroscopePersistService _horoscopePersistService;

        #endregion

        public Horoscope() {
            _zodiacs = new List<ZodiacSign>();
            _horoscopePersistService = new HoroscopePersistService();

            // Create the 12 zodiacs.
            CreateZodiacs();
        }

        //TODO: Write XAML comments.
        public List<ZodiacSign> ZodiacSigns {
            get { return _zodiacs; }
        }

        //TODO: Write XAML comments.
        private void CreateZodiacs() {
            // [0] = Sign; [1] = StartDate; [2] = Predictions.
            for (int i = 0; i < 12; i++) {
                ZodiacSign sign = new ZodiacSign(_horoscopePersistService.Store[i, 0], _horoscopePersistService.Store[i, 1], _horoscopePersistService.Store[i, 2]);

                _zodiacs.Add(sign);
            }
        }

        //TODO: Write XAML comments.
        private string DeterminePrediction(int dayOfYear) {
            for (int i = 0; i < (_zodiacs.Count - 1); i++) {
                //TODO: Ensure accuracy. I know I got lazy.
                // Validate start and end dates of zodiacs.
                if ((dayOfYear >= _zodiacs[i].StartDate[0]) && (dayOfYear < _zodiacs[i+1].StartDate[0])) {
                    return _zodiacs[i].Name.ToString();
                }
            }

            // Error.
            return null;
        }
    }
}
