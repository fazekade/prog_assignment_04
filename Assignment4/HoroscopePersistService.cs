﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment4 {
    class HoroscopePersistService {

        #region Field variables

        //TODO: Write XML comment.
        private string[,] _predictionStore;

        #endregion

        public HoroscopePersistService() {
            LoadFile();
        }

        #region Properties

        //TODO: Write XML comment.
        public string[,] Store {
            get { return _predictionStore; }
        }

        #endregion

        #region Private methods

        //TODO: Write XML comment.
        private async void SaveFile(List<string> content) {
            Windows.Storage.StorageFolder storageFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile file = null;

            // Get 'prediction-store.dat', or create if not found.
            file = await storageFolder.CreateFileAsync("prediction-store.dat", Windows.Storage.CreationCollisionOption.OpenIfExists);
            //sampleFile = await storageFolder.GetFileAsync("prediction-store.dat");

            // Write the updated predictions to file.
            using (var stream = await file.OpenAsync(Windows.Storage.FileAccessMode.ReadWrite)) {
                await Windows.Storage.FileIO.AppendLinesAsync(file, content);
            }

        }

        //TODO: Write XML comment.
        private async void LoadFile() {
            //TODO: Create default horoscopes if none found, otherwise use found ones.
            
            // Create local storage folder.
            Windows.Storage.StorageFolder folder = Windows.Storage.ApplicationData.Current.LocalFolder;

            // Create local storage file.
            //Windows.Storage.StorageFile file = await folder.GetFileAsync("prediction-store.dat");
            Windows.Storage.StorageFile file = await folder.CreateFileAsync("prediction-store.dat", Windows.Storage.CreationCollisionOption.OpenIfExists);

            var buffer = await Windows.Storage.FileIO.ReadBufferAsync(file);
            using (var dataReader = Windows.Storage.Streams.DataReader.FromBuffer(buffer)) {
                string text = dataReader.ReadString(buffer.Length);
                string[] content = text.Split('\n');
                _predictionStore = new string[12, 3];

                for (int i = 0; i < content.Length; i++) {
                    string[] sign = content[i].Split('|');
                    _predictionStore[i, 0] = sign[0];
                    _predictionStore[i, 1] = sign[1];
                    _predictionStore[i, 2] = "No predictions exist!";

                    if (sign[2] != "\r") {
                        _predictionStore[i, 2] = sign[2];
                    }
                }
                
            }
        }

        #endregion
    }
}
